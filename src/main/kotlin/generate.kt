import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import java.io.File
import java.io.PrintStream

fun main() {
    val printStream = PrintStream(File("public/index.html").outputStream())
    printStream.appendln("<!DOCTYPE html>")
    printStream.appendHTML().html {
        head {
            meta { charset = "utf-8" }
            meta { name = "generator"; content = "kotlin" }
            meta { name = "viewport"; content = "width=device-width, initial-scale=1.0, user-scalable=yes" }
            title("winniehell")
        }

        body {
            p { +"Hi, I’m Winnie!" }
        }
    }
}
